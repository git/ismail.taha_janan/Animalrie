#include "animale.h"

Animale::Animale(QString name, QObject *dad)
    :QObject(dad),
      m_name(name)
{

}

const QString &Animale::name() const
{
    return m_name;
}

void Animale::setName(const QString &newName)
{
    if (m_name == newName)
        return;
    m_name = newName;
    emit nameChanged();
}

int Animale::height() const
{
    return m_height;
}

void Animale::setHeight(int newHeight)
{
    if (m_height == newHeight)
        return;
    m_height = newHeight;
    emit heightChanged();
}
