#ifdef QT_QML_DEBUG
#include <QtQuick>
#endif

#include "animale.h"

#include <sailfishapp.h>

#include <QGuiApplication>
#include <QQuickView>
#include <QScopedPointer>
#include <QQmlContext>

int main(int argc, char *argv[])
{
    // SailfishApp::main() will display "qml/Animalerie.qml", if you need more
    // control over initialization, you can use:
    //
    //   - SailfishApp::application(int, char *[]) to get the QGuiApplication *
    //   - SailfishApp::createView() to get a new QQuickView * instance
    //   - SailfishApp::pathTo(QString) to get a QUrl to a resource file
    //   - SailfishApp::pathToMainQml() to get a QUrl to the main QML file
    //
    // To display the view, call "show()" (will show fullscreen on device).

    QScopedPointer<QGuiApplication> app {SailfishApp::application(argc, argv)};

    qmlRegisterType<Animale>("Prey",1,0,"Animal");

    QScopedPointer<QQuickView> view {SailfishApp::createView()};
    QString ironmaiden="1976";
    view->rootContext()->setContextProperty("musique", ironmaiden);

    view->setSource(SailfishApp::pathToMainQml());

    view->show();



    return app->exec();
}
