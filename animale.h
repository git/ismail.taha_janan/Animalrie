#ifndef ANIMALE_H
#define ANIMALE_H

#include <QObject>
#include <QString>


class Animale: public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(int height READ height WRITE setHeight NOTIFY heightChanged)
    QString m_name;

    int m_height;

public:
    Animale(QString name = "Unknown", QObject * dad = nullptr);
    const QString &name() const;
    void setName(const QString &newName);
    int height() const;
    void setHeight(int newHeight);

signals:
    void nameChanged();
    void heightChanged();
};

#endif // ANIMALE_H
