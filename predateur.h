#ifndef PREDATEUR_H
#define PREDATEUR_H

#include <QObject>
#include "animale.h"

class Predateur: public Animale
{



    Q_PROPERTY(int caninesLong READ caninesLong WRITE setCaninesLong NOTIFY caninesLongChanged)

    int m_caninesLong;

public:
    Predateur(QString name="",int caninesLong = 10,QObject *parent = nullptr);

    QString name() const;
    int caninesLong() const;

public slots:
    void setName(QString name);
    void setHeight(int height);
    void setCaninesLong(int longeur);

signals:

    void nameChanged(QString name);
    void heightChanged(int height);
    void caninesLongChanged(int longeur);


};

#endif // PREDATEUR_H
